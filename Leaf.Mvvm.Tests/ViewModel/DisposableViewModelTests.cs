using System;
using System.Threading;
using Leaf.Tests.Fakes;
using Leaf.ViewModel;
using Moq;
using Moq.Protected;
using NUnit.Framework;

namespace Leaf.Tests.ViewModel
{
    [TestFixture]
    public class DisposableViewModelTests
    {
        [Test]
        public void Ctor_HappyFlow_CreatesDisposableInstance()
        {
            var sut = new DisposableViewModelStub();
            Assert.IsInstanceOf<IDisposable>(sut);
        }

        [Test]
        public void Ctor_HappyFlow_CreatesViewModel()
        {
            var sut = new DisposableViewModelStub();
            Assert.IsInstanceOf<Leaf.ViewModel.ViewModel>(sut);
        }

        [Test]
        public void Dispose_CalledDirectly_CallsProtectedWithDisposingFlagTrue()
        {
            var mock = new Mock<DisposableViewModel> {CallBase = true};
            IProtectedMock<DisposableViewModel> protMock = mock.Protected();

            mock.Object.Dispose();

            protMock.Verify("Dispose", Times.Once(), true);
        }

        [Test]
        public void Dispose_CalledTwice_DoesNotCallProtectedMethodTwice()
        {
            var mock = new Mock<DisposableViewModel> {CallBase = true};
            IProtectedMock<DisposableViewModel> protMock = mock.Protected();

            mock.Object.Dispose();
            mock.Object.Dispose();

            protMock.Verify("Dispose", Times.Once(), true);
        }

        [Test]
        public void Dispose_CalledTwice_IsThreadSafe()
        {
            var mock = new Mock<DisposableViewModel> {CallBase = true};
            IProtectedMock<DisposableViewModel> protMock = mock.Protected();
            var t1 = new Thread(() =>
                {
                    Thread.Sleep(500);
                    mock.Object.Dispose();
                });
            t1.Start();
            mock.Object.Dispose();

            t1.Join();

            protMock.Verify("Dispose", Times.Once(), true);
        }

        [Test]
        public void GC_Never_ThrowsObjectDisposedException()
        {
            {
                var sut = new DisposableViewModelStub();
                sut.Dispose();
            }

            Assert.DoesNotThrow(() =>
                {
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                });
        }
    }
}