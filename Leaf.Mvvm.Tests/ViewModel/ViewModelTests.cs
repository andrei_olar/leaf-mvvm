using Leaf.Tests.Fakes;
using NUnit.Framework;

namespace Leaf.Tests.ViewModel
{
    [TestFixture]
    public class ViewModelTests
    {
        [Test]
        public void Ctor_HappyFlow_CreatesNewModelInstance()
        {
            var sut = new ViewModelStub();

            Assert.IsInstanceOf<Leaf.Model.Model>(sut);
        }
    }
}