using Leaf.Tests.Fakes;
using NUnit.Framework;
using System.ComponentModel;

namespace Leaf.Tests.Model
{
    [TestFixture]
    public class ModelTests
    {
        [Test]
        public void Ctor_HappyFlow_CreatesNewInpcImplementation()
        {
            var model = new ModelStub();

            Assert.IsInstanceOf<INotifyPropertyChanged>(model);
        }

        [Test]
        public void SomeProperty_ChangeValue_RaisesPropertyChanged()
        {
            var model = new ModelStub();
            bool raised = false;
            model.PropertyChanged += (s, e) => raised = true;

            model.SomeProperty = new object();

            Assert.IsTrue(raised);
        }

        [Test]
        public void SomeProperty_SetSameValueTwice_DoesNotRaisePropertyChanges()
        {
            var value = new object();
            var model = new ModelStub
                {
                    SomeProperty = value
                };
            bool raised = false;
            model.PropertyChanged += (s, e) => raised = true;

            model.SomeProperty = value;

            Assert.IsFalse(raised);
        }

        [Test]
        public void SomeProperty_UnsubscribePropertyChanged_DoesNotRaisePropertyChanges()
        {
            var value = new object();
            var model = new ModelStub {SomeProperty = value};
            bool raised = false;
            PropertyChangedEventHandler handler = (s, e) => raised = true;
            model.PropertyChanged += handler;

            model.PropertyChanged -= handler;
            model.SomeProperty = value;

            Assert.IsFalse(raised);
        }
    }
}