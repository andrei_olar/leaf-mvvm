namespace Leaf.Tests.Fakes
{
    public sealed class ModelStub : Leaf.Model.Model
    {
        private object someField;

        public object SomeProperty
        {
            get { return someField; }
            set
            {
                SetPropertyValue<ModelStub, object>(
                    x => x.SomeProperty, ref someField, value);
            }
        }
    }
}