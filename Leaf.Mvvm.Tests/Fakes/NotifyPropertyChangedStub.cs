using System.ComponentModel;

namespace Leaf.Tests.Fakes
{
    internal sealed class NotifyPropertyChangedStub : INotifyPropertyChanged
    {
        internal object SomeField = null;

        internal object SomeProperty { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}