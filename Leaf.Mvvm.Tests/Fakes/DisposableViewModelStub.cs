using Leaf.ViewModel;

namespace Leaf.Tests.Fakes
{
    internal sealed class DisposableViewModelStub : DisposableViewModel
    {
        protected override void Dispose(bool disposing)
        {
        }
    }
}