using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Threading;
using Leaf.Tests.Fakes;
using NUnit.Framework;

namespace Leaf.Tests
{
    public class PropertyChangedNotifierTests
    {
        [Test]
        public void Ctor_HappyFlow_CreatesNewInstance()
        {
            Console.WriteLine(new PropertyChangedNotifier(null));
        }

        [Test]
        public void BoastPropertyChanged_NullPropertySelector_ThrowsArgumentNullException()
        {
            var sut = new PropertyChangedNotifier(null);
            object val = null;
            Assert.Throws<ArgumentNullException>(
                () => sut.BoastPropertyChanged<INotifyPropertyChanged, object>(null, ref val, val));
        }

        [Test]
        public void BoastPropertyChanged_NewValueIsDifferentFromOldValue_RaisesPropertyChanged()
        {
            bool raised = false;
            var sut = new PropertyChangedNotifier(null);
            sut.PropertyChanged += (sender, e) => raised = true;
            object old = null;

            sut.BoastPropertyChanged<NotifyPropertyChangedStub, object>(
                x => x.SomeProperty, ref old, new object());

            Assert.IsTrue(raised);
        }

        [Test]
        public void BoastPropertyChanged_NewValueIsDifferentFromOldValue_RaisedPropertyChangeHasExpectedSender()
        {
            var expected = new NotifyPropertyChangedStub();
            var sut = new PropertyChangedNotifier(expected);
            NotifyPropertyChangedStub actual = null;
            sut.PropertyChanged += (sender, e) => actual = (NotifyPropertyChangedStub) sender;
            object old = null;

            sut.BoastPropertyChanged<NotifyPropertyChangedStub, object>(
                x => x.SomeProperty, ref old, new object());

            Assert.AreSame(expected, actual);
        }

        [Test]
        public void BoastPropertyChanged_NewValueIsDifferentFromOldValue_RaisedPropertyChangeHasExpectedPropertyName()
        {
            Expression<Func<NotifyPropertyChangedStub, object>> expr = x => x.SomeProperty;
            string expected = expr.GetPropertyName();
            var sut = new PropertyChangedNotifier(null);
            string actual = null;
            sut.PropertyChanged += (sender, e) => actual = e.PropertyName;
            object old = null;

            sut.BoastPropertyChanged<NotifyPropertyChangedStub, object>(
                x => x.SomeProperty, ref old, new object());

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void BoastPropertyChanged_NewValueIsDifferentFromOldValue_OldValueReceivesNewValue()
        {
            var sut = new PropertyChangedNotifier(null);
            object old = null;
            var @new = new object();

            sut.BoastPropertyChanged<NotifyPropertyChangedStub, object>(
                x => x.SomeProperty, ref old, @new);

            Assert.AreSame(@new, old);
        }

        [Test]
        public void BoastPropertyChanged_NewValueIsEqualToOldValue_PropertyChangedIsNotRaised()
        {
            bool raised = false;
            var sut = new PropertyChangedNotifier(null);
            sut.PropertyChanged += (s, e) => raised = true;
            var old = new object();
            object @new = old;

            sut.BoastPropertyChanged<NotifyPropertyChangedStub, object>(
                x => x.SomeProperty, ref old, @new);

            Assert.IsFalse(raised);
        }

        [Test]
        public void BoastPropertyChanged_SetSameValueTwice_IsThreadSafe()
        {
            var value = new object();
            var sut = new PropertyChangedNotifier(null);
            object old = null;
            bool raised = false;
            var t1 = new Thread(() =>
                {
                    Thread.Sleep(500);
                    sut.PropertyChanged += (s, e) => raised = true;
                    sut.BoastPropertyChanged<NotifyPropertyChangedStub, object>(
                        x => x.SomeProperty, ref old, value);
                });
            t1.Start();
            sut.BoastPropertyChanged<NotifyPropertyChangedStub, object>(
                x => x.SomeProperty, ref old, value);

            t1.Join();

            Assert.IsFalse(raised);
        }
    }
}