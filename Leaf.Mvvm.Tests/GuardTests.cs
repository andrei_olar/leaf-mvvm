using System;
using NUnit.Framework;
using Guard = Leaf.Guard;

namespace Leaf.Tests
{
    [TestFixture]
    public class GuardTests
    {
        [Test]
        public void ArgNotNull_HappyFlow_IsFastEnough()
        {
            var arg = new object();
            DateTime d1 = DateTime.Now;
            for (int i = 0; i < 1000000; i++)
            {
                Guard.ArgNotNull(arg, () => arg);
            }
            DateTime d2 = DateTime.Now;
            Assert.LessOrEqual(d2.Subtract(d1).Milliseconds, 1000);
        }

        [Test]
        public void ArgNotNull_IfArgNotNull_DoesNotThrow()
        {
            var arg = new object();
            Guard.ArgNotNull(arg, () => arg);
        }

        [Test]
        public void ArgNotNull_IfArgNull_ThrowsArgNullException()
        {
            object arg = null;

            var exc = Assert.Throws<ArgumentNullException>(
                () => Guard.ArgNotNull(arg, () => arg));
            Assert.That(exc.Message, Contains.Substring("arg"));
        }
    }
}