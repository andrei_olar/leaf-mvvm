using System;
using System.Linq.Expressions;
using Leaf.NameResolution;
using NUnit.Framework;

namespace Leaf.Tests.NameResolution
{
    [TestFixture]
    public sealed class ResolverTests
    {
        [Test]
        public void GetName_HappyFlow_ThrowsNameResolutionException()
        {
            object o = null;
            var sut = new Resolver();
            Expression<Func<object>> lambda = () => o;
            Assert.Throws<NameResolutionException>(() => sut.GetName(lambda));
        }

        [Test]
        public void GetName_NullLambda_ThrowsArgumentNullException()
        {
            var sut = new Resolver();
            Assert.Throws<ArgumentNullException>(
                () => sut.GetName(null));
        }
    }
}