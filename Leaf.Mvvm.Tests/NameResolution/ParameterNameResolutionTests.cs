using NUnit.Framework;
using System;
using System.Linq.Expressions;

namespace Leaf.Tests.NameResolution
{
    [TestFixture(TypeArgs = new[] {typeof (object)})]
    public class ParameterNameResolutionTests
    {
        [TestCase("a string")]
        public void GetParamName_HappyFlow_ReturnsLocalVarName(string param)
        {
            const string expected = "param";
            Expression<Func<string>> lambda = () => param;

            string actual = lambda.GetParamName();

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetParamName_MemberNotLambdaExpression_ThrowsNameResolutionException()
        {
            Expression<Func<bool>> anExpr = () => false;
            Assert.Throws<NameResolutionException>(() => anExpr.GetParamName());
        }
    }
}