using System;
using System.Linq.Expressions;
using Leaf.NameResolution;
using Leaf.Tests.Fakes;
using NUnit.Framework;

namespace Leaf.Tests.NameResolution
{
    [TestFixture]
    public class PropertyNameResolutionTests
    {
        [Test]
        public void GetPropertyName_ExpressionIsNotMemberExpression_ThrowsNameResolutionException()
        {
            Expression<Func<NotifyPropertyChangedStub, object>> expr = x => new object();

            Assert.Throws<NameResolutionException>(() => expr.GetPropertyName());
        }

        [Test]
        public void GetPropertyName_HappyFlow_ReturnsPropertyName()
        {
            const string expected = "SomeProperty";
            Expression<Func<NotifyPropertyChangedStub, object>> expr = x => x.SomeProperty;

            string actual = expr.GetPropertyName();

            Assert.AreEqual(expected, actual);
        }
    }
}