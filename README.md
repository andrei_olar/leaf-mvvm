#Leaf MVVM
The __Model-View-ViewModel__ design pattern is widely used in the .NET development community. This is just an implementation of the bare minimum to get started with it.

#Build Status
[![Build status](https://ci.appveyor.com/api/projects/status/mojsbvflswlcig9b?svg=true)](https://ci.appveyor.com/project/koosie0507/leaf-mvvm)

#Features
__Leaf MVVM__ provides:

* property change notification;
* base classes for common usage scenarios;
* disposable viewmodels;

__Leaf MVVM__ does not do and will *never* do:

* dependency resolution;
* magic string recognition in XAML;
* binding sugar;
* RX sugar;