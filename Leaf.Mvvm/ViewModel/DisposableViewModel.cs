using System;

namespace Leaf.ViewModel
{
    public abstract class DisposableViewModel : ViewModel, IDisposable
    {
        private static readonly object SyncRoot = new object();
        private bool disposed;

        public void Dispose()
        {
            lock (SyncRoot)
            {
                if (disposed)
                    return;

                Dispose(true);
                disposed = true;
            }
            GC.SuppressFinalize(this);
        }

        protected abstract void Dispose(bool disposing);

        ~DisposableViewModel()
        {
            Dispose(false);
        }
    }
}