using System;
using System.Linq.Expressions;
using Leaf.NameResolution;

namespace Leaf
{
    public static class Guard
    {
        private static readonly MemberResolver nameResolver;

        static Guard()
        {
            nameResolver = new MemberResolver();
        }

        public static void ArgNotNull<T>(T value, Expression<Func<T>> argSelector)
        {
            if (null == value)
                throw new ArgumentNullException(nameResolver.GetName(argSelector));
        }
    }
}