using System;
using System.ComponentModel;
using System.Linq.Expressions;

namespace Leaf
{
    public sealed class PropertyChangedNotifier
    {
        private static readonly object SyncRoot = new object();
        private readonly INotifyPropertyChanged onBehalfOf;

        public PropertyChangedNotifier(INotifyPropertyChanged onBehalfOf)
        {
            this.onBehalfOf = onBehalfOf;
        }

        public void RaisePropertyChanged<T, TProp>(
            Expression<Func<T, TProp>> propertySelector)
            where T : INotifyPropertyChanged
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null == handler)
                return;

            string propertyName = propertySelector.GetPropertyName();
            handler(onBehalfOf, new PropertyChangedEventArgs(propertyName));
        }

        public void BoastPropertyChanged<T, TProp>(
            Expression<Func<T, TProp>> propertySelector,
            ref TProp oldValue,
            TProp newValue)
            where T : INotifyPropertyChanged
        {
            Guard.ArgNotNull(propertySelector, () => propertySelector);

            lock (SyncRoot)
            {
                if (Equals(oldValue, newValue))
                    return;

                oldValue = newValue;
                RaisePropertyChanged(propertySelector);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}