using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("Leaf MVVM")]
[assembly: AssemblyDescription("Leaf Model-View-ViewModel Framework")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Andrei Olar")]
[assembly: AssemblyProduct("Leaf")]
[assembly: AssemblyCopyright("Andrei Olar")]
[assembly: AssemblyTrademark("Andrei Olar")]
[assembly: AssemblyVersion("1.0.1.0")]
[assembly: InternalsVisibleTo("Leaf.Tests")]