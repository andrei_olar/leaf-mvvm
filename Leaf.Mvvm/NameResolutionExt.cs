using System;
using System.ComponentModel;
using System.Linq.Expressions;
using Leaf.NameResolution;

namespace Leaf
{
    public static class NameResolutionExt
    {
        public static string GetPropertyName<T, TProp>(
            this Expression<Func<T, TProp>> propertySelector)
            where T : INotifyPropertyChanged
        {
            return new PropertyResolver().GetName(propertySelector);
        }

        public static string GetParamName<T>(
            this Expression<Func<T>> paramSelector)
        {
            return new MemberResolver().GetName(paramSelector);
        }
    }
}