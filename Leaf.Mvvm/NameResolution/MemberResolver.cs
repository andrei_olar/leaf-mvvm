using System.Linq.Expressions;

namespace Leaf.NameResolution
{
    internal sealed class MemberResolver : Resolver
    {
        protected override string GetName(Expression lambda)
        {
            if (ExpressionType.MemberAccess == lambda.NodeType)
            {
                var body = (MemberExpression) lambda;
                return body.Member.Name;
            }
            return base.GetName(lambda);
        }
    }
}