using System.Linq.Expressions;
using System.Reflection;

namespace Leaf.NameResolution
{
    internal sealed class PropertyResolver : Resolver
    {
        protected override string GetName(Expression lambda)
        {
            if (ExpressionType.MemberAccess == lambda.NodeType)
            {
                var expression = (MemberExpression) lambda;
                return expression?.Member?.Name;
            }
            return base.GetName(lambda);
        }
    }
}