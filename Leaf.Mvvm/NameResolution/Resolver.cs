using System.Linq.Expressions;

namespace Leaf.NameResolution
{
    /// <summary>
    ///     Instances of this class are given lambda expressions
    ///     on which they resolve names.
    /// </summary>
    public class Resolver
    {
        protected virtual string GetName(Expression lambda)
        {
            throw new NameResolutionException();
        }

        public string GetName(LambdaExpression lambda)
        {
            Guard.ArgNotNull(lambda, () => lambda);
            return GetName(lambda.Body);
        }
    }
}