using System;
using System.ComponentModel;
using System.Linq.Expressions;

namespace Leaf.Model
{
    public abstract class Model : INotifyPropertyChanged
    {
        private readonly PropertyChangedNotifier boaster;

        protected Model()
        {
            boaster = new PropertyChangedNotifier(this);
        }

        public event PropertyChangedEventHandler PropertyChanged
        {
            add { boaster.PropertyChanged += value; }
            remove { boaster.PropertyChanged -= value; }
        }

        protected void SetPropertyValue<TModel, TProperty>(
            Expression<Func<TModel, TProperty>> propertySelector,
            ref TProperty oldValue,
            TProperty newValue)
            where TModel : Model
        {
            boaster.BoastPropertyChanged(
                propertySelector, ref oldValue, newValue);
        }
    }
}